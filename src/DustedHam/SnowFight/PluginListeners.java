package DustedHam.SnowFight;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PluginListeners implements Listener {
	
	Plug plug;
	
	public PluginListeners(Plug plug)
	{
		this.plug = plug;	
	}
		
	@EventHandler
	public void DamagedByEvent(EntityDamageByEntityEvent event)
	{	
		if(event.getEntity() instanceof Player && event.getDamager() instanceof Snowball)
		{
			//Check if Player is Playing and hit by a Snow ball
			Player player = (Player) event.getEntity();
			Snowball ball = (Snowball) event.getDamager();
			Player shooter = (Player) ball.getShooter();
			if(plug.isPlaying(player.getName()) && plug.isPlaying(shooter.getName()))
			{
				if(!plug.isAllied(player, shooter) || plug.friendlyFire)
				{
					plug.onHit(player, shooter);
				}
			}
		}
		else if(event.getEntity() instanceof Player && event.getDamager() instanceof Player)
		{
			Player player1 = (Player) event.getEntity();
			Player player2 = (Player) event.getDamager();
			
			if(plug.isPlaying(player1.getName()) && plug.isPlaying(player2.getName()))
			{
				if(!plug.isAllied(player1, player2) || plug.friendlyFire)
				{
					plug.onHit(player1, player2);
				}
			}
		}
	}
	
	@EventHandler
	public void QuitEvent(PlayerQuitEvent event)
	{
		plug.Quit(event.getPlayer());
	}
	
	@EventHandler
	public void DeathEvent(PlayerDeathEvent event)	
	{
		Player player = (Player) event.getEntity();
		if(plug.isPlaying(player.getName()))
		{
			player.getInventory().clear();
			event.getDrops().clear();
		}
	}
	@EventHandler
	public void SpawnEvent(PlayerRespawnEvent event)
	{
		Player player = event.getPlayer();
		if(plug.isPlaying(player.getName()))
		{
			player.sendMessage(ChatColor.RED + "You Died While playing SnowBall Fight.... How ?");		
			//Respawn Player in SBF 5 ticks after they Respawn Normal(Its weird but it was needed)
			plug.getServer().getScheduler().scheduleSyncDelayedTask(plug, new RespawnTime(plug, player), 5);	
		}
	}
	
	@EventHandler
	public void BlockBrokenEvent(BlockBreakEvent event) {
       Player player = event.getPlayer();
       //Check if player is playing
       if(plug.isPlaying(player.getName()))
       {
    	   //DO NOT ALLOW PLAYERS TO BREAK BLOCKS
    	   event.setCancelled(true);
       }
    }
}
