package DustedHam.SnowFight;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class Plug extends JavaPlugin {

	//----Variables
	boolean friendlyFire = true;
	boolean elimination = false;
	boolean teamScramble = true;
	int maxScore = 50;
	int SpawnTimeSeconds = 3;
	//--Teams
	public ArrayList<String> RedTeam;
	public ArrayList<String> BlueTeam;
	int RedScore = 0;
	int BlueScore = 0;
	//--Locations
	Location RedSpawn;
	Location RedLobby;
	Location BlueSpawn;
	Location BlueLobby;
	HashMap<String, PlayerData> PlayerDatas;
	//--Random Number
	Random rnd;
	//--Files
	File RedSpawnLocation;
	File RedLobbyLocation;
	File BlueSpawnLocation;
	File BlueLobbyLocation;
	
	//Snow Fight Methods
	boolean isPlaying(String playerName)
	{
		if(RedTeam.contains(playerName))
		{
			return true;
		}
		else if(BlueTeam.contains(playerName))
		{
			return true;
		}
		return false;	
	}	
	
	void onHit(Player victim, Player thrower)
	{
		//Team Scoring
		if (!isAllied(victim, thrower))
		{
			if(RedTeam.contains(thrower.getName()))
			{
				RedScore ++;
			}
			else
			{
				BlueScore ++;
			}
		}
				
		//Drop Snow
		dropSnow(victim);
		
		if(elimination && RedLobby != null && BlueLobby != null)
		{
			if(RedTeam.contains(victim.getName()))
			{
				victim.teleport(RedLobby);
			}
			else
			{
				victim.teleport(BlueLobby);
			}
					
			if (RedScore >= BlueTeam.size() || BlueScore >= RedTeam.size())
			{
				endRound();
			}
		}
		else
		{
			//Delay or not
			if(RedLobby != null && BlueLobby != null && SpawnTimeSeconds > 0)
			{
				if(RedTeam.contains(victim.getName()))
				{
					victim.teleport(RedLobby);
				}
				else
				{
					victim.teleport(BlueLobby);
				}
				
				getServer().getScheduler().scheduleSyncDelayedTask(this, new RespawnTime(this, victim), SpawnTimeSeconds * 20);			
			}
			else
			{
				Respawn(victim);
			}	
			if (RedScore >= maxScore || BlueScore >= maxScore)
			{
				endRound();
			}
		}
	}
	
	void endRound()
	{
		 String winner;
		 if (BlueScore > RedScore)
		 {
			 winner = "Blue Team";
		 }
		 else if (RedScore > BlueScore)
		 {
			 winner = "Red Team";
		 }
		 else
		 {
			 winner = "Tie?";
		 }
		 	   	 	   	 
	   	 //Respawn and Declare winner
	   	 for(int i = 0; i < RedTeam.size(); i++)
	   	 {
	   		 String Name = RedTeam.get(i);
	   		 Player player;
	   		 if((player = getServer().getPlayer(Name)) != null)
	   		 {
	   			 player.sendMessage(ChatColor.GREEN + "Round is over! Winner is " + winner);
	   		 }
	   		 Respawn(player);
	   	 }
	   	 for(int i = 0; i < BlueTeam.size(); i++)
	   	 {
	   		 String Name = BlueTeam.get(i);
	   		 Player player;
	   		 if((player = getServer().getPlayer(Name)) != null)
	   		 {
	   			 player.sendMessage(ChatColor.GREEN + "Round is over! Winner is " + winner);
	   		 }
	   		Respawn(player);
	   	 }
	   	 
	   	 if(teamScramble)
	   	 {
	   		TeamScramble();
	   	 }
	   	 
	   	 RedScore = 0;
	   	 BlueScore = 0;
	}
	
	void messageAll(String msg)
	{
  		for(int i = 0;i < RedTeam.size(); i++)
  		{
  			Player player;
  			if((player = getServer().getPlayer(RedTeam.get(i))) != null)
	   		{
  				player.sendMessage(msg);
	   		}
  		} 
  		for(int i = 0;i < BlueTeam.size(); i++)
  		{
  			Player player;
  			if((player = getServer().getPlayer(RedTeam.get(i))) != null)
	   		{
  				player.sendMessage(msg);
	   		}
  		}
	}
	
	void TeamScramble()
	{	 
		Queue<String> PlayerQueue = new LinkedList<String>();
   		 
  		//Add all players to a Queue
  		for(int i = 0;i < RedTeam.size(); i++)
  		{
  			PlayerQueue.add(RedTeam.get(i));
  		} 
  		for(int i = 0;i < BlueTeam.size(); i++)
  		{
  			PlayerQueue.add(BlueTeam.get(i));
  		}
  		 
  		//Clear Team Lists
  		BlueTeam.clear();
  		RedTeam.clear();
  		 
  	    //Scramble Teams
  		String playerName;
  		while((playerName = PlayerQueue.poll()) != null)
  		{
  			Player player;
  			if((player = getServer().getPlayer(playerName)) != null)
	   		{
  				if(RedTeam.size() > BlueTeam.size())
  				{
  					//Join Blue
  					BlueTeam.add(playerName);
  		   			player.sendMessage(ChatColor.GREEN + "Teams have been scrambled you are now on " + "Blue Team");
  				}
  				else if(RedTeam.size() < BlueTeam.size())
  				{
  					//Join Red
  					RedTeam.add(playerName);
  		   			player.sendMessage(ChatColor.GREEN + "Teams have been scrambled you are now on " + "Red Team");
  				}
  				else
  				{
  					//Join Random
  					if(rnd.nextBoolean())
  					{
  						RedTeam.add(playerName);
  			   			player.sendMessage(ChatColor.GREEN + "Teams have been scrambled you are now on " + "Red Team");
  					}
  					else
  					{
  						BlueTeam.add(playerName);
  			   			player.sendMessage(ChatColor.GREEN + "Teams have been scrambled you are now on " + "Blue Team");
  					}
  				}
  				Respawn(player);
	   		}
  		} 
	}	
	public boolean isAllied(Player p1, Player p2)
	{
		if (RedTeam.contains(p1.getName()) && RedTeam.contains(p2.getName()))
		{
			return (true);
		}
		else if ((BlueTeam.contains(p1.getName()) && BlueTeam.contains(p2.getName())))
		{
			return (true);
		}
		return (false);
	}
	
	ItemStack colorize(ItemStack item, Color color)
	{
		LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
		meta.setColor(color);
		item.setItemMeta(meta);	
		return item;	
	}
	
	void dropSnow(Player player)
	{
		ItemStack[] inv = player.getInventory().getContents();
		
		for(ItemStack item : inv)
		{
			if(item != null && item.getType() == Material.SNOW_BALL)
			{
				player.getWorld().dropItem(player.getEyeLocation(), item);
			}
		}
		player.getInventory().clear();
	}
	
	public void Respawn(Player player)
	{	
		player.getInventory().clear();
		//Teleport && Team Colors
		Color color = Color.GREEN;
		if(RedTeam.contains(player.getName()))
		{
			//Move to Spawn
			color = Color.RED;
			player.teleport(RedSpawn);
		}
		else if(BlueTeam.contains(player.getName()))
		{
			//Move to Spawn
			color = Color.BLUE;
			player.teleport(BlueSpawn);
		}
		//Give Armour
		player.getInventory().setBoots(colorize(new ItemStack(Material.LEATHER_BOOTS,1),color));
		player.getInventory().setLeggings(colorize(new ItemStack(Material.LEATHER_LEGGINGS,1),color));
		player.getInventory().setChestplate(colorize(new ItemStack(Material.LEATHER_CHESTPLATE,1),color));
		player.getInventory().setHelmet(colorize(new ItemStack(Material.LEATHER_HELMET,1),color));
		//Give Snow
		player.getInventory().addItem(new ItemStack(Material.SNOW_BALL, 16));
		player.setHealth(player.getMaxHealth());
		player.setFoodLevel(100);	
	}
	
	void Play(Player player)
	{
		//Check if Playing
		if(!isPlaying(player.getName()))
		{
			if(RedTeam.size() > BlueTeam.size())
			{
				//Join Blue
				BlueTeam.add(player.getName());
			}
			else if(RedTeam.size() < BlueTeam.size())
			{
				//Join Red
				RedTeam.add(player.getName());
			}
			else
			{
				//Join Random
				if(rnd.nextBoolean())
				{
					RedTeam.add(player.getName());
				}
				else
				{
					BlueTeam.add(player.getName());
				}
			}
			//Save Player Data
			PlayerDatas.put(player.getName(), new PlayerData(player));
			player.setGameMode(GameMode.ADVENTURE);
			//Clear Inventory
			player.getInventory().clear();
			//Spawn Player
			Respawn(player);
		}
		else
		{
			player.sendMessage(ChatColor.RED + "You are already playing Snow Ball Fight");
		}
	}	

	void Quit(Player player)
	{
		if(isPlaying(player.getName()))
		{
			//Quit team
			if(RedTeam.contains(player.getName()))
			{
				RedTeam.remove(player.getName());
			}
			else if(BlueTeam.contains(player.getName()))
			{
				BlueTeam.remove(player.getName());
			}
			//Restore Player Data
			PlayerData pd = PlayerDatas.get(player.getName());
			player.setHealth(pd.Health);
			player.setFoodLevel(pd.FoodLevel);
			player.teleport(pd.Location);
			player.getInventory().clear();
			player.getInventory().setContents(pd.Inventory);
			player.getInventory().setArmorContents(pd.ArmorContents);
			player.setGameMode(pd.gameMode);
		}
		else
		{
			player.sendMessage(ChatColor.RED + "You are not playing Snow Ball Fight");
		}
	}
	
	void Switch(Player player)
	{
		if(isPlaying(player.getName()))
		{
			String name = player.getName();
			
			if(RedTeam.contains(name) && RedTeam.size() > BlueTeam.size())
			{
				RedTeam.remove(name);
				BlueTeam.add(name);
				Respawn(player);
			}
			else if(BlueTeam.contains(name) && BlueTeam.size() > RedTeam.size())
			{
				BlueTeam.remove(name);
				RedTeam.add(name);
				Respawn(player);
			}
			else
			{
				player.sendMessage(ChatColor.RED + "You Cannot Switch Teams");
			}
		}
	}
	//------------------
	
	void SaveLocation(Location spawnpoint, File file) throws Exception
	{
		if(spawnpoint != null)
		{
			FileWriter filestream = new FileWriter (file, false);
			BufferedWriter out = new BufferedWriter(filestream);
		
			out.write(spawnpoint.getWorld().getName());
			out.newLine();
		
			out.write(String.valueOf(spawnpoint.getBlockX()));
			out.newLine();	
		
			out.write(String.valueOf(spawnpoint.getBlockY()));
			out.newLine();
		
			out.write(String.valueOf(spawnpoint.getBlockZ()));
			out.newLine();
		
			out.write(String.valueOf(spawnpoint.getPitch()));
			out.newLine();
		
			out.write(String.valueOf(spawnpoint.getYaw()));
			out.newLine();
		
			out.close();
			filestream.close();
		}
	}
	
	Location LoadLocation(File file) throws Exception
	{		
		if(!file.exists())
		{
			file.createNewFile();
			getLogger().info("Spawn File Created");
		}
		else
		{
			DataInputStream input = new DataInputStream(new FileInputStream(file));
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			
			//Variables
			String worldName = "";
			Double X = 0.0;
			Double Y = 0.0;
			Double Z = 0.0;
			float Pitch = 0.0f;
			float Yaw = 0.0f;
			
			String line;
			int count = 0;
			while((line = reader.readLine()) !=null)
			{
				if(count == 0)
				{
					worldName = line;
				}
				else if(count == 1)
				{
					X = Double.valueOf(line);
				}
				else if(count == 2)
				{
					Y = Double.valueOf(line);
				}
				else if(count == 3)
				{
					Z = Double.valueOf(line);
				}
				else if(count == 4)
				{
					Pitch = Float.valueOf(line);
				}	
				else if(count == 5)
				{
					Yaw = Float.valueOf(line);
				}
				count ++;
			}
			
			reader.close();
			input.close();
			if(worldName != "" || X != 0 || Y != 0 || Z != 0 || Pitch != 0 || Yaw != 0)
			{
				Location SpawnLocation = new Location(getServer().getWorld(worldName), X, Y, Z,Yaw, Pitch);
				return SpawnLocation;
			}		
		}
		return null;
	}
	
	@Override
	public void onEnable()
	{			
		//initialize
		rnd = new Random();
		RedTeam = new ArrayList<String>();
		BlueTeam = new ArrayList<String>();
		PlayerDatas = new HashMap<String,PlayerData>();		
		getServer().getPluginManager().registerEvents(new PluginListeners(this), this);		
		//load Spawn Locations
		if(getDataFolder().exists())
		{
			RedSpawnLocation = new File(getDataFolder(), "RedSpawnLocation.txt");
			BlueSpawnLocation = new File(getDataFolder(), "BlueSpawnLocation.txt");
			RedLobbyLocation = new File(getDataFolder(),"RedLobbyLocation.txt");
			BlueLobbyLocation = new File(getDataFolder(),"BlueLobbyLocation.txt");
			
			try {
				RedSpawn = LoadLocation(RedSpawnLocation);
				BlueSpawn = LoadLocation(BlueSpawnLocation);
				RedLobby = LoadLocation(RedLobbyLocation);
				BlueLobby = LoadLocation(BlueLobbyLocation);
			} catch (Exception e) {
				e.printStackTrace();
			}			
		}
		else
		{
			getDataFolder().mkdirs();
		}
	}

	
    @Override
    public void onDisable()
    {
    	//Save Spawn Locations
    	try {
			SaveLocation(RedSpawn,RedSpawnLocation);
			SaveLocation(BlueSpawn,BlueSpawnLocation);
			SaveLocation(RedLobby,RedLobbyLocation);
			SaveLocation(BlueLobby,BlueLobbyLocation);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	//Force Quit Each Player
    	for(int i = 0; i < RedTeam.size(); i ++)
    	{
    		Quit(Bukkit.getPlayer(RedTeam.get(i)));	
    	}
    	for(int i = 0; i < BlueTeam.size(); i ++)
    	{
    		Quit(Bukkit.getPlayer(BlueTeam.get(i)));	
    	}
    }
    
    @Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		//--Player Commands--//
    	if (sender instanceof Player)
		{
			Player player = (Player) sender;			
			if(cmd.getName().equalsIgnoreCase("SBF"))
    		{
    			if(args.length >= 1)
    			{
    				if(args[0].equalsIgnoreCase("Play"))
    				{
    					if((RedSpawn != null) && (BlueSpawn != null))
    					{
    						//Join Game
    						Play(player);				
    					}
    					else
    					{
    						if((RedSpawn == null) && (BlueSpawn == null))							
    						{
    							player.sendMessage(ChatColor.RED + "Red AND Blue team spawns are not set");
    						}
    						else if(RedSpawn == null)
    						{
    							player.sendMessage(ChatColor.RED + "Red Team spawn is not set");
    						}
    						else if(BlueSpawn == null)
    						{
    							player.sendMessage(ChatColor.RED + "Blue Team spawn is not set");
    						}
    					}
    					return true;
    				}
    				else if(args[0].equalsIgnoreCase("Quit"))
    				{
    					//Quit Game
    					Quit(player);
    					return true;
    				}
    				else if(args[0].equalsIgnoreCase("Switch"))
    				{
    					//Quit Game
    					Switch(player);
    					return true;
    				}
    				else
    				{
    					player.sendMessage(ChatColor.RED + "Not Valid Snow Ball Fight Command");
    					return true;
    				}
    			}
				else
				{
					player.sendMessage(ChatColor.RED + "Not Valid Snow Ball Fight Command");
					return true;
				}
    		}
			else if(cmd.getName().equalsIgnoreCase("SBFAdmin"))
			{
				if(args.length >= 1)
				{
					if(args[0].equalsIgnoreCase("Set"))
					{
						if(args.length >= 2)
						{
							if(args[1].equalsIgnoreCase("RedSpawn"))
							{
								RedSpawn = player.getEyeLocation();
								player.sendMessage(ChatColor.GREEN + "Red spawn Set");
								return true;
							}
							else if(args[1].equalsIgnoreCase("BlueSpawn"))
							{
								BlueSpawn = player.getEyeLocation();
								player.sendMessage(ChatColor.GREEN + "Blue spawn Set");
								return true;
							}
							else if(args[1].equalsIgnoreCase("BlueLobby"))
							{
								BlueLobby = player.getEyeLocation();
								player.sendMessage(ChatColor.GREEN + "Blue lobby Set");
								return true;
							}
							else if(args[1].equalsIgnoreCase("RedLobby"))
							{
								RedLobby = player.getEyeLocation();
								player.sendMessage(ChatColor.GREEN + "Red lobby Set");
								return true;
							}
							else if(args[1].equalsIgnoreCase("Lobby"))
							{
								RedLobby = player.getEyeLocation();
								BlueLobby = player.getEyeLocation();
								player.sendMessage(ChatColor.GREEN + "Blue and Red lobby Set");
								return true;
							}
		    				else
		    				{
		    					player.sendMessage(ChatColor.RED + "Not Valid Snow Ball Fight Command");
		    					return true;
		    				}
						}
	    				else
	    				{
	    					player.sendMessage(ChatColor.RED + "Not Valid Snow Ball Fight Command");
	    					return true;
	    				}
					}
    				else if(args[0].equalsIgnoreCase("FF"))
    				{
    					player.sendMessage(ChatColor.GREEN + "Friendly Fire is now: " + toggleFF());
    					return true;
    				}
					else if(args[0].equalsIgnoreCase("SpawnTime"))
					{
						if(args.length > 0)
						{		
							int Seconds = Integer.parseInt(args[1]);
							if(Seconds >= 0)
							{
								SpawnTimeSeconds = Seconds;
								player.sendMessage(ChatColor.GREEN + "Respawn Time is Now " + Seconds);
							}
							else
							{
								player.sendMessage(ChatColor.RED + "Spawn Time Must be 0 seconds or Greater");
							}
							return true;
						}
					}
					else if(args[0].equalsIgnoreCase("AutoScramble"))
					{
						if(args.length > 0)
						{
							player.sendMessage(ChatColor.GREEN + "Team Scramble is now: " + toggleScramble());
							return true;
						}
					}
					else if(args[0].equalsIgnoreCase("Scramble"))
					{
						if(args.length > 0)
						{
							TeamScramble();
							player.sendMessage(ChatColor.GREEN + "Teams have Been Scrambled");
							return true;
						}
					}
					else if(args[0].equalsIgnoreCase("Elimination"))
					{
						if(args.length > 0)
						{
							elimination = !elimination;
							player.sendMessage(ChatColor.GREEN + "Elimination Mode is now: " + elimination);
							return true;
						}
					}
    				else
    				{
    					player.sendMessage(ChatColor.RED + "Not Valid Snow Ball Fight Command");
    					return true;
    				}
				}
				else
				{
					player.sendMessage(ChatColor.RED + "Not Valid Snow Ball Fight Command");
					return true;
				}
			}
		}
    	else
    	{
			if(cmd.getName().equalsIgnoreCase("SBF"))
    		{
    			if(args.length > -1)
    			{
    				if(args[0].equalsIgnoreCase("Play"))
    				{
    	    			if(args.length > 0)
    	    			{
    	    				Player player = getServer().getPlayer(args[1]);
    	    				Play(player);
    	    				return true;
    	    			}
    				}
    				else if(args[0].equalsIgnoreCase("Quit"))
    				{
    	    			if(args.length > 0)
    	    			{
    	    				Player player = getServer().getPlayer(args[1]);
    	    				Quit(player);
    	    				return true;
    	    			}
    				}
    				//Sorry bros this is for my Twitch Bot
    				else if(args[0].equalsIgnoreCase("TwitchScore"))
    				{				
    					if(RedScore > BlueScore)
    					{
    						Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
    								"twitch say Red Team is Wining " + RedScore + " to " + BlueScore);
    					}
    					else if(BlueScore > RedScore)
    					{
    						Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
    								"twitch say Blue Team is Wining " +BlueScore + " to " + RedScore);
    					}
    					else
    					{
    						Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
    								"twitch say The Game is Tied " + BlueScore + " to " + RedScore);
    					}
    					return true;
    				}
    			}
    		}
    	}
		return false;	
	}
    
    boolean toggleFF()
    {
    	friendlyFire = !friendlyFire;
    	String enabledOrDisabled = "enabled";
    	if (friendlyFire == false)
    	{
    		enabledOrDisabled = "disabled";
    	}
    	
    	messageAll(ChatColor.GREEN + "Team Scramble is now " + enabledOrDisabled);
    	
    	return friendlyFire;
    }
    
    boolean toggleScramble()
    {
    	teamScramble = !teamScramble;
    	String enabledOrDisabled = "enabled";
    	if (teamScramble == false)
    	{
    		enabledOrDisabled = "disabled";
    	}  
    	
    	messageAll(ChatColor.GREEN + "Team Scramble is now " + enabledOrDisabled);
    	
    	return teamScramble;
    }
}
