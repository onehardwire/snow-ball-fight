package DustedHam.SnowFight;

import org.bukkit.entity.Player;


public class RespawnTime implements Runnable  {

	//--Variables
	Player player;
	Plug plug;
	
	public RespawnTime(Plug plug, Player player)
	{
		this.player = player;
		this.plug = plug;
	}
	
	@Override
	public void run() {
		
		//Respawn Player
		plug.Respawn(player);
	}
}
