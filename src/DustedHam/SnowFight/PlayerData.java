package DustedHam.SnowFight;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.GameMode;
import org.bukkit.Location;

public class PlayerData {

	//Variables
	public int Health;
	public int FoodLevel;
	public Location Location;
	public ItemStack[] Inventory;
	public ItemStack[] ArmorContents;
	public GameMode gameMode;
	
	
	public PlayerData(Player player)
	{
		this.Health = player.getHealth();
		this.FoodLevel = player.getFoodLevel();
		this.Location = player.getLocation();
		this.Inventory = player.getInventory().getContents();
		this.ArmorContents = player.getInventory().getArmorContents();
		this.gameMode = player.getGameMode();
	}
}
